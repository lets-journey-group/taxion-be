﻿using Microsoft.EntityFrameworkCore;
using Taxion.BE.Business;
using Taxion.BE.Data.EntityConfigurations;
using Taxion.BE.Services.Interfaces;

namespace Taxion.BE.Data;

public class TaxionContext : DbContext, ITaxionContext
{
    public DbSet<Auction> Auctions { get; set; }

    public DbSet<Driver> Drivers { get; set; }

    public DbSet<Passenger> Passengers { get; set; }

    public DbSet<Journey> Journeys { get; set; }

    public TaxionContext()
    {
        Database.EnsureCreated();
    }

    public TaxionContext(DbContextOptions<TaxionContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new AuctionEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new JourneyEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
    }

    void ITaxionContext.Add<TEntity>(TEntity entity)
    {
        base.Add(entity);
    }

    public async Task SaveChangesAsync()
    {
        await base.SaveChangesAsync();
    }
}
