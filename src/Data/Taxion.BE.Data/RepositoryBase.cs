﻿namespace Taxion.BE.Data;

public abstract class RepositoryBase
{
    protected readonly TaxionContext Context;

    public RepositoryBase(TaxionContext context)
    {
        ArgumentNullException.ThrowIfNull(context);
        this.Context = context;
    }
}
