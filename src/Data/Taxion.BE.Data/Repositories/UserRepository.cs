﻿using Taxion.BE.Business;
using Taxion.BE.Services.Interfaces.Repositories;

namespace Taxion.BE.Data.Repositories;

public class UserRepository : RepositoryBase, IUserRepository
{
    public UserRepository(TaxionContext context) : base(context)
    {
    }

    public async Task<Passenger?> RetrievePassengerOrDefaultAsync(Guid userId)
    {
        return await Context.Passengers.FindAsync(userId);
    }

    public async Task<Passenger> RetrievePassengerAsync(Guid userId)
    {
        return await RetrievePassengerOrDefaultAsync(userId) ??
            throw new InvalidOperationException($"Passenger with id '{userId}' not found.");
    }

    public async Task<Driver?> RetrieveDriverOrDefaultAsync(Guid userId)
    {
        return await Context.Drivers.FindAsync(userId);
    }

    public async Task<Driver> RetrieveDriverAsync(Guid userId)
    {
        return await RetrieveDriverOrDefaultAsync(userId) ??
            throw new InvalidOperationException($"Driver with id '{userId}' not found.");
    }
}
