﻿namespace Taxion.BE.Api.Configuration;

public class AppSettings
{
    public ConnectionStrings ConnectionStrings { get; init; }
}
