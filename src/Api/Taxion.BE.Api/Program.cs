using Microsoft.EntityFrameworkCore;
using Taxion.BE.Api.Configuration;
using Taxion.BE.Api.Filters;
using Taxion.BE.Api.ServiceProviderExtensions;
using Taxion.BE.Data;
using Taxion.BE.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

var appSettings = builder.Configuration.Get<AppSettings>()
    ?? throw new InvalidOperationException("Invalid configuration mapping.");

builder.AddLogging(appSettings);

// Add services to the container.
builder.Services.AddDbContext<ITaxionContext, TaxionContext>(options =>
{
    options.UseNpgsql(appSettings.ConnectionStrings.JourneyDB);
});

builder.Services.AddControllers(options =>
{
    options.SuppressAsyncSuffixInActionNames = false;
    options.Filters.Add<ExceptionFilterAttribute>();
});

builder.Services.AddRouting(opt => opt.LowercaseUrls = true);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCors();

builder.Services.AddSwagger();

builder.Services.AddUseCaseServices();
builder.Services.AddGatewayServices();

//builder.Services.AddAuthenticationAndAuthorization(appSettings);

var app = builder.Build();

// Configure the HTTP request pipeline.

//if (app.Environment.IsDevelopment())
if (true)
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();
app.UseCors(builder =>
{
    builder.AllowAnyOrigin();
    builder.AllowAnyMethod();
    builder.AllowAnyHeader();
});

//HACK: commented for environment setting up and testing
//if (app.Environment.IsProduction())
//{
//    app.UseAuthentication();
//    app.UseAuthorization();

//    app.MapControllers()
//        .RequireAuthorization("ApiScope");
//}
//else
//{
app.MapControllers();
//}

app.Run();

public partial class Program { }