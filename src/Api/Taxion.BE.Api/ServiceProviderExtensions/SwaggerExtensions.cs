﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Taxion.BE.Api.ServiceProviderExtensions;

public static class SwaggerExtensions
{
    public static void AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            ConvertAttributes(options);
            SetupSecurity(options);
        });
    }

    private static void ConvertAttributes(SwaggerGenOptions options)
    {
        options.CustomSchemaIds(x =>
        {
            var attribute = x.GetCustomAttributes<DisplayAttribute>().SingleOrDefault();
            return attribute is null ? x.Name : attribute.Name;
        });
        options.CustomOperationIds(
                description => description.ActionDescriptor is not ControllerActionDescriptor actionDescriptor
                    ? null
                    : actionDescriptor.ActionName);
    }

    private static void SetupSecurity(SwaggerGenOptions options)
    {
        options.AddSecurityDefinition("jwt_auth1", new OpenApiSecurityScheme
        {
            Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
            Name = "Bearer",
            In = ParameterLocation.Header,
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            Scheme = "bearer",
        });
        OpenApiSecurityScheme securityScheme = new()
        {
            Reference = new OpenApiReference()
            {
                Id = "jwt_auth1",
                Type = ReferenceType.SecurityScheme
            }
        };
        OpenApiSecurityRequirement securityRequirements = new()
        {
            {
                securityScheme,
                Array.Empty<string>()
            },
        };

        options.AddSecurityRequirement(securityRequirements);
    }
}
