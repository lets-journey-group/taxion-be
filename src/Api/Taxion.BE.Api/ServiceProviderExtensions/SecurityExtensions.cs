﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Taxion.BE.Api.Configuration;

namespace Taxion.BE.Api.ServiceProviderExtensions;

public static class SecurityExtensions
{
    public static void AddAuthenticationAndAuthorization(this IServiceCollection services, AppSettings appSettings)
    {
        AddAuthentication(services, appSettings);
        AddAuthorization(services);
    }

    private static void AddAuthentication(IServiceCollection services, AppSettings appSettings)
    {
        services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.Authority = appSettings.ConnectionStrings.Identity;
                options.TokenValidationParameters = new()
                {
                    ValidateAudience = false
                };
                options.RequireHttpsMetadata = false;
            });
    }

    private static void AddAuthorization(IServiceCollection services)
    {
        services.AddAuthorization(options =>
        {
            options.AddPolicy("ApiScope", policy =>
            {
                policy.RequireAuthenticatedUser();
                policy.RequireClaim("scope", "LetsJourneyWebApi");
            });
        });
    }
}
