﻿using Serilog;
using Taxion.BE.Api.Configuration;

namespace Taxion.BE.Api.ServiceProviderExtensions;

public static class LoggingExtensions
{
    public static void AddLogging(this WebApplicationBuilder builder, AppSettings appSettings)
    {
        SetupLogs(appSettings);
        builder.Logging
            .ClearProviders()
            .AddSerilog();
    }

    private static void SetupLogs(AppSettings appSettings)
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .WriteTo.Seq(appSettings.ConnectionStrings.Seq)
            .CreateLogger();
    }
}
