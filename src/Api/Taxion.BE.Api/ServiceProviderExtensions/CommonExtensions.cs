﻿using Taxion.BE.Data.Repositories;
using Taxion.BE.Services.Interfaces.Repositories;
using Taxion.BE.Services.UseCases.Driver;
using Taxion.BE.Services.UseCases.Passenger;

namespace Taxion.BE.Api.ServiceProviderExtensions;

public static class CommonExtensions
{
    public static void AddUseCaseServices(this IServiceCollection services)
    {
        services.AddScoped<ManageAuctionUseCase>();
        services.AddScoped<ParticipateAuctionUseCase>();
    }

    public static void AddGatewayServices(this IServiceCollection services)
    {
        services.AddScoped<IAuctionRepository, AuctionRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
    }
}
