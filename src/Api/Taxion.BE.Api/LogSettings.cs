﻿using Serilog;
using Taxion.BE.Api.Configuration;

namespace Taxion.BE.Api;

public static class LogSettings
{
    public static void SetupLogs(AppSettings appSettings)
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Information()
            .Enrich.FromLogContext()
            .WriteTo.Console()
            .WriteTo.Seq(appSettings.ConnectionStrings.Seq)
            .CreateLogger();
    }
}
