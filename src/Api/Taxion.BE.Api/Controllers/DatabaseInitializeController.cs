﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Taxion.BE.Business;
using Taxion.BE.Data;

namespace Taxion.BE.Api.Controllers;

/// <summary>
/// Only for testing environments.
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class DatabaseInitializeController : ControllerBase
{
    private readonly Guid passengerUserGuid = new("FCDE5C9E-69D1-4193-B569-A4143CD78B2D");
    private readonly Guid driverUserGuid = new("A208F0A4-0766-441B-B070-D67F53D18E7F");
    private readonly TaxionContext context;

    public DatabaseInitializeController(
        TaxionContext taxionContext)
    {
        this.context = taxionContext;
    }

    /// <summary>
    /// Only for testing environments.
    /// Creates a test users for database.
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateAsync()
    {
        var passenger = Mock.Of<Passenger>(d => d.Id == passengerUserGuid);
        var driver = Mock.Of<Driver>(d => d.Id == driverUserGuid);

        context.Passengers.Add(passenger);
        context.Drivers.Add(driver);
        await context.SaveChangesAsync();

        return Ok();
    }
}
