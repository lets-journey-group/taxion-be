﻿using Taxion.BE.Services.Interfaces;

namespace Taxion.BE.Services.UseCases;

public abstract class UseCaseBase
{
    protected ITaxionContext JourneyContext;

    public UseCaseBase(ITaxionContext journeyContext)
    {
        this.JourneyContext = journeyContext;
    }
}
