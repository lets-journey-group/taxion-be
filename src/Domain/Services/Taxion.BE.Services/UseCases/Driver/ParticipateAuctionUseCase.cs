﻿using Taxion.BE.Business;
using Taxion.BE.Services.Interfaces;
using Taxion.BE.Services.Interfaces.Repositories;

namespace Taxion.BE.Services.UseCases.Driver;

public class ParticipateAuctionUseCase : UseCaseBase
{
    private readonly IAuctionRepository auctionRepository;
    private readonly IUserRepository userRepository;

    public ParticipateAuctionUseCase(
        IAuctionRepository auctionRepository,
        IUserRepository driverRepository,
        ITaxionContext journeyContext) : base(journeyContext)
    {
        this.auctionRepository = auctionRepository;
        this.userRepository = driverRepository;
    }

    public async Task<IList<Auction>> GetAuctionsAsync()
    {
        return await auctionRepository.RetrieveAllAsync();
    }

    public async Task<Auction?> GetAuctionOrDefaultAsync(int auctionId)
    {
        return await auctionRepository.RetrieveOrDefaultAsync(auctionId);
    }

    public async Task GoInAuctionAsync(int auctionId, decimal bet, Guid userId)
    {
        var driver = await userRepository.RetrieveDriverAsync(userId);
        var auction = await auctionRepository.RetrieveAsync(auctionId);

        auction.AddCandidate(driver, bet);

        await JourneyContext.SaveChangesAsync();
    }
}
