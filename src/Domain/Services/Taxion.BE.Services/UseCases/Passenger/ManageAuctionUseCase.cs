﻿using Taxion.BE.Business;
using Taxion.BE.Services.Interfaces;
using Taxion.BE.Services.Interfaces.Repositories;

namespace Taxion.BE.Services.UseCases.Passenger;

public class ManageAuctionUseCase : UseCaseBase
{
    private readonly IAuctionRepository auctionRepository;
    private readonly IUserRepository userRepository;

    public ManageAuctionUseCase(
        IAuctionRepository auctionRepository,
        IUserRepository userRepository,
        ITaxionContext journeyContext) : base(journeyContext)
    {
        this.auctionRepository = auctionRepository;
        this.userRepository = userRepository;
    }

    public async Task<Auction> CreateAuctionAsync(string fromStreet, string toStreet, Guid userId)
    {
        var passenger = await userRepository.RetrievePassengerAsync(userId);

        var auction = Auction.Create(passenger, fromStreet, toStreet);

        JourneyContext.Add(auction);
        await JourneyContext.SaveChangesAsync();
        return auction;
    }

    public async Task CreateJourneyWithChipestCandidateAsync(Guid userId)
    {
        var auction = await auctionRepository.RetrieveByPassengerAsync(userId);
        var journey = auction.CreateJourneyWithChipestCandidate();

        JourneyContext.Add(journey);
        await JourneyContext.SaveChangesAsync();
    }
}
