﻿using Taxion.BE.Business;

namespace Taxion.BE.Services.Interfaces.Repositories;

public interface IAuctionRepository
{
    Task<List<Auction>> RetrieveAllAsync();

    Task<Auction?> RetrieveOrDefaultAsync(int id);

    Task<Auction> RetrieveAsync(int id);

    Task<Auction> RetrieveByPassengerAsync(Guid userId);

    Task<Auction> RetrieveByPassengerAsync(Passenger passenger);
}
