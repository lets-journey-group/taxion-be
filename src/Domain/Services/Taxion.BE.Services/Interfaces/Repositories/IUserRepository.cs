﻿using Taxion.BE.Business;

namespace Taxion.BE.Services.Interfaces.Repositories;

public interface IUserRepository
{
    Task<Passenger?> RetrievePassengerOrDefaultAsync(Guid userId);

    Task<Passenger> RetrievePassengerAsync(Guid userId);

    Task<Driver?> RetrieveDriverOrDefaultAsync(Guid userId);

    Task<Driver> RetrieveDriverAsync(Guid userId);
}
