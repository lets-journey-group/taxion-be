﻿namespace Taxion.BE.Services.Interfaces;

public interface ITaxionContext
{
    void Add<TEntity>(TEntity entity) where TEntity : class;

    Task SaveChangesAsync();
}
