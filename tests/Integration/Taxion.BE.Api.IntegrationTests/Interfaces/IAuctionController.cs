﻿using Refit;

namespace Taxion.BE.Api.IntegrationTests.Interfaces;

public interface IAuctionController
{
    [Get("/api/auctions")]
    Task<IApiResponse<List<DTO.Output.Auction>>> GetAllAsync();

    [Get("/api/auctions/{id}")]
    Task<IApiResponse<DTO.Output.Auction>> GetAsync(int id);

    [Post("/api/auctions")]
    Task<IApiResponse<DTO.Output.Auction>> CreateAsync([Body] DTO.Input.Auction auction);

    [Post("/api/auctions/{auctionId}/candidates")]
    Task<IApiResponse<DTO.Output.Auction>> AddCandidateAsync(int auctionId, [Body] decimal bet);
}
