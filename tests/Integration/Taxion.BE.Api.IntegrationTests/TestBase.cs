﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Refit;
using Taxion.BE.Api.IntegrationTests.Interfaces;
using Taxion.BE.Data;

namespace Taxion.BE.Api.IntegrationTests;

public abstract class TestBase : IClassFixture<CustomWebApplicationFactory<Program>>, IDisposable
{
    protected readonly CustomWebApplicationFactory<Program> Factory;
    protected readonly TaxionContext Context;
    protected readonly HttpClient HttpClient;
    protected readonly IAuctionController AuctionClient;

    protected TestBase(CustomWebApplicationFactory<Program> factory)
    {
        Factory = factory;
        Context = factory.GetContext();
        HttpClient = factory.CreateClient(new WebApplicationFactoryClientOptions
        {
            AllowAutoRedirect = false
        });

        AuctionClient = RestService.For<IAuctionController>(HttpClient);
    }

#pragma warning disable CA1816
    public void Dispose()
    {
        ClearDatabase();
    }
#pragma warning restore CA1816

    private void ClearDatabase()
    {
        string getTablesQuery = @"SELECT name 
                                      FROM sqlite_schema 
                                      WHERE type ='table' 
                                      AND name NOT LIKE 'sqlite_%';";

        var tableNames = Context.Database
            .SqlQueryRaw<string>(getTablesQuery)
            .ToList();

        foreach (var tableName in tableNames)
        {
            Context.Database.ExecuteSqlRaw(string.Format("DELETE FROM {0}", tableName));
        }

        Context.SaveChanges();
    }
}
